package main

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/pborman/getopt"
	"github.com/tebeka/selenium"
	"gitlab.com/Alvoras/hostel/logger"
)

var (
	visible *bool
	full    *bool
)

func init() {
	visible = getopt.BoolLong("visible", 'd', "false", "Download only the visible accordion")
	full = getopt.BoolLong("full", 'f', "true", "Download all the courses of the page")
}

func main() {
	var wd selenium.WebDriver
	var err error
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	// TODO:
	// use selenium.AddFirefox() to add a firefox.Capabilities{} struct
	// Add firefox preferences
	// profile.setPreference("browser.helperApps.neverAsk.saveToDisk" , "application/octet-stream;application/csv;text/csv;application/vnd.ms-excel;");
	// profile.setPreference("browser.helperApps.alwaysAsk.force", false);
	// profile.setPreference("browser.download.manager.showWhenStarting",false);
	// profile.setPreference("browser.download.folderList", 2);
	// profile.setPreference("browser.download.dir",OUTDIR FULL PATH);

	wd, err = selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", 8080))
	if err != nil {
		fmt.Println("Error while querying the myges page", err)
		return
	}

	if err := wd.Get("https://www.myges.fr/student/courses-files"); err != nil {
		fmt.Println("Error while querying the myges page", err)
		return
	}

	connectSSO(wd)

	if *visible {
		fmt.Println("Press enter when you're ready to download the visible links on myges.fr...")
		bufio.NewReader(os.Stdin).ReadBytes('\n')

		visibleAccordion, err := wd.FindElement(selenium.ByCSSSelector, `.ui-accordion-content[aria-hidden="false"]`)
		if err != nil {
			fmt.Println("Error finding visible accordion", err)
			return
		}

		dlBtns, err := visibleAccordion.FindElements(selenium.ByCSSSelector, "button")
		if err != nil {
			fmt.Println("Error finding download buttons", err)
			return
		}
		fmt.Println(len(dlBtns))

		cnt := 0
		for _, btn := range dlBtns {
			cnt++
			fmt.Println(extractURLFromButton(btn))
			btn.Click()
			if cnt == 2 {
				break
			}
		}
	} else {
		accordions, err := wd.FindElements(selenium.ByCSSSelector, `.ui-accordion-content`)
		if err != nil {
			fmt.Println("Error finding visible accordion", err)
			return
		}

		for _, accordion := range accordions {
			fmt.Println(accordion.Click())
			dlBtns, err := accordion.FindElements(selenium.ByCSSSelector, "button")
			if err != nil {
				fmt.Println("Error finding download buttons", err)
				return
			}

			fmt.Println(len(dlBtns))

			cnt := 0
			for _, btn := range dlBtns {
				cnt++
				fmt.Println(extractURLFromButton(btn))
				fmt.Println(btn.Click())

				if cnt == 2 {
					break
				}
				fmt.Println(accordion.Click())

			}

		}

		// if err := wd.Get(url); err != nil {
		// 	fmt.Println("Error while querying the download cas page", err)
		// 	return
		// }

		// go connectSSO(wd)

		// filepath := fmt.Sprintf("out%d", cnt)
		//
		// err = downloadFile(filepath, url)
		// if err != nil {
		// 	fmt.Println("Error downloading file", err)
		// 	return
		// }
	}

	fmt.Println("OK")
}

func downloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func connectSSO(wd selenium.WebDriver) {
	usernameInput, err := wd.FindElement(selenium.ByID, "username")
	if err != nil {
		logger.Error.Println("Error finding the username element :", err)
		os.Exit(1)
	}
	passwordInput, err := wd.FindElement(selenium.ByID, "password")
	if err != nil {
		logger.Error.Println("Error finding the password element :", err)
		os.Exit(1)
	}
	connectBtn, err := wd.FindElement(selenium.ByName, "submit")
	if err != nil {
		logger.Error.Println("Error finding the login button :", err)
		os.Exit(1)
	}

	err = usernameInput.SendKeys("xxx")
	if err != nil {
		logger.Error.Println("Error filling the username field :", err)
		os.Exit(1)
	}

	err = passwordInput.SendKeys("xxx")
	if err != nil {
		logger.Error.Println("Error filling the password field :", err)
		os.Exit(1)
	}

	err = connectBtn.Click()
	if err != nil {
		logger.Error.Println("Error clicking the login button :", err)
		os.Exit(1)
	}

}

func extractURLFromButton(btn selenium.WebElement) (string, error) {
	js, err := btn.GetAttribute("onclick")
	if err != nil {
		fmt.Println("Error getting js code", err)
		return "", err
	}

	urlTrimmedLeft := strings.Split(js, "('")
	urlTrimmeRight := strings.Split(urlTrimmedLeft[1], "')")
	url := urlTrimmeRight[0]
	return url, nil
}
